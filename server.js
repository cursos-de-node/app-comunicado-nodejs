const express = require('express');
const app = express();
const sql = require('mssql');
const bodyParser = require('body-parser');

//const middleware = require('./middlewares/middlewares');

sql.on('error', err => {
    console.error('Error SQL: ', err);
});

require('./config/config');
require('./helpers/helpers');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.set('view engine', 'hbs');

app.get('/:id', /*middleware.autenticado, */ (req, res) => {
    let identityUsuario = req.params.id;

    sql.connect(process.env.URLDB).then(pool => {
        return pool.request()
            .input('cd_identityUsuario', sql.Int, identityUsuario)
            .execute('sps_ObtieneComunicadoUsuario_Node')
    }).then(result => {
        if (result.recordset[0] === undefined) {
            res.render('index', {
                plantilla: 'SinComunicado'
            });
        } else {
            console.log("Resultado GET:", result.recordset[0]);

            res.render('index', {
                identityUsuario: result.recordset[0].cd_identityUsuario,
                nombre: result.recordset[0].nb_nombre,
                identityComunicado: result.recordset[0].cd_identityGrupoComunicado,
                plantilla: result.recordset[0].nb_plantilla,
                titulo: result.recordset[0].tx_titulo,
                subTitulo: result.recordset[0].tx_subTitulo,
                parrafoUno: result.recordset[0].tx_parrafoUno,
                parrafoDos: result.recordset[0].tx_parrafoDos,
                parrafoTres: result.recordset[0].tx_parrafoTres,
                imagenUno: result.recordset[0].tx_imagenUno,
                imagenDos: result.recordset[0].tx_imagenDos,
                piePagina: result.recordset[0].tx_piePagina
            });
        }
    }).catch(err => {
        console.error("Error GET: ", err);
    });

});

app.post('/:id', (req, res) => {
    let identityUsuario = req.params.id;
    let valor = req.body.btn_opcion.split('|');

    ejecutaConsultaDB(process.env.URLDB, `update TRSch_GrupoComunicado_Node set st_grupoComunicado = 'N', tx_respuesta = '${valor[0]}' where cd_identityGrupoComunicado = ${valor[1]}`);

    sql.connect(process.env.URLDB).then(pool => {
        return pool.request()
            .input('cd_identityUsuario', sql.Int, identityUsuario)
            .execute('sps_ObtieneComunicadoUsuario_Node')
    }).then(result => {
        if (result.recordset[0] === undefined) {
            res.render('index', {
                plantilla: 'SinComunicado'
            });
        } else {
            console.log("Resultado POST:", result.recordset[0]);

            res.render('index', {
                identityUsuario: result.recordset[0].cd_identityUsuario,
                nombre: result.recordset[0].nb_nombre,
                identityComunicado: result.recordset[0].cd_identityGrupoComunicado,
                plantilla: result.recordset[0].nb_plantilla,
                titulo: result.recordset[0].tx_titulo,
                subTitulo: result.recordset[0].tx_subTitulo,
                parrafoUno: result.recordset[0].tx_parrafoUno,
                parrafoDos: result.recordset[0].tx_parrafoDos,
                parrafoTres: result.recordset[0].tx_parrafoTres,
                imagenUno: result.recordset[0].tx_imagenUno,
                imagenDos: result.recordset[0].tx_imagenDos,
                piePagina: result.recordset[0].tx_piePagina
            });
        }
    }).catch(err => {
        console.error("Error POST: ", err);
    });
});

function ejecutaConsultaDB(config, query) {
    sql.connect(config).then(pool => {
        return pool.request()
            .query(query)
    }).then(result => {
        console.log("Resultado Tabla", result);
    }).catch(err => {
        console.error("Error: ", err);
    });
}

function ejecutaStoreProcedureDB(config, store, value) {

    sql.connect(config).then(pool => {
        return pool.request()
            .input('cd_identityUsuario', sql.Int, value)
            .execute(store)
    }).then(result => {
        if (result.recordset[0] === undefined) {
            res.render('index', {
                plantilla: 'SinComunicado'
            });
        } else {
            console.log("Resultado POST:", result.recordset[0]);

            res.render('index', {
                identityUsuario: result.recordset[0].cd_identityUsuario,
                nombre: result.recordset[0].nb_nombre,
                identityComunicado: result.recordset[0].cd_identityGrupoComunicado,
                plantilla: result.recordset[0].nb_comunicado,
                comunicado: result.recordset[0].tx_descripcion
            });
        }
    }).catch(err => {
        console.error("Error POST: ", err);
    });

}

app.listen(process.env.PORT, () => {
    console.log(`Escuchando desde el puerto ${process.env.PORT}`);
});