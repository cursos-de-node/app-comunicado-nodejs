 const hbs = require('hbs');

 hbs.registerHelper('ifEquals', function(arg1, arg2, options) {
     return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
 });

 hbs.registerHelper("interpretaHtml", function(options) {

     let mensaje = options.fn(this);

     //(<)
     while (mensaje.includes("&lt;")) {
         mensaje = mensaje.replace("&lt;", "<");
     }
     //(>)
     while (mensaje.includes("&gt;")) {
         mensaje = mensaje.replace("&gt;", ">");
     }
     //(=)
     while (mensaje.includes("&#x3D;")) {
         mensaje = mensaje.replace("&#x3D;", '=');
     }
     //(")
     while (mensaje.includes("&quot;")) {
         mensaje = mensaje.replace("&quot;", '"');
     }

     //console.log('sHelper', mensaje);

     return new hbs.SafeString(mensaje);
 });

 //  hbs.registerHelper('actualizaValor', function(nombre, options) {

 //      console.log(options.fn(this), nombre);

 //      return options.fn(this);
 //  });